//
//  Item+CoreDataClass.swift
//  DreamLister
//
//  Created by Mustafa GUNES on 20.01.2018.
//  Copyright © 2018 Mustafa GUNES. All rights reserved.
//

import Foundation
import CoreData


public class Item: NSManagedObject {
    
    public override func awakeFromInsert() {
        
        super.awakeFromInsert()
        self.created = NSDate()
    }
}
