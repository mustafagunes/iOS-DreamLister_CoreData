//
//  ItemType+CoreDataProperties.swift
//  DreamLister
//
//  Created by Mustafa GUNES on 20.01.2018.
//  Copyright © 2018 Mustafa GUNES. All rights reserved.
//

import Foundation
import CoreData


extension ItemType {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemType> {
        return NSFetchRequest<ItemType>(entityName: "ItemType");
    }

    @NSManaged public var type: String?
    @NSManaged public var toItem: Item?

}
